openapi: 3.0.0
servers:
  - description: localTicketing Discovery Api
    url: http://api.localticketing.io/discovery
  - description: localTicketing Discovery Api for frontend
    url: http://localticketing.io/api/discovery
info:
  description: Discovery api for showing events, venues, organizers etc.
  version: "1.0.0"
  title: Discovery API
  contact:
    email: info@localticketing.de
tags:
  - name: events
  - name: collections
  - name: agencies
  - name: genres
  - name: search
paths:
  /events:
    get:
      tags:
        - events
      description: Shows all events
      parameters:
        - in: query
          name: collection_id
          required: false
          schema: 
            type: integer
          description: Shows only events that are connected to this collection
        - in: query
          name: organizer_id
          schema:
            type: integer
          description: Shows only events that are connected to this organizer.
        - in: query
          name: venue_id
          required: false
          schema:
            type: integer
          description: Shows only events that are connected to this venue.
        - in: query
          name: genre_id
          required: false
          schema:
            type: integer
          description: Shows only events that are connected to this category.
        - in: query
          name: start_date_from
          required: false
          schema:
            type: string
          description: Shows only events that are after / same as the start_date
        - in: query
          name: start_date_to
          required: false
          schema:
            type: string
          description: Shows only events that are before / same as the start_date
        - in: query
          name: end_date_from
          required: false
          schema:
            type: string
          description: Shows only events that are after / same as the end_date
        - in: query
          name: end_date_to
          required: false
          schema:
            type: string
          description: Shows only events that are before / same as the end_date
        - in: query
          name: page
          required: false
          schema:
            type: integer
          description: Page of the chuncked results
        - in: query
          name: per_page
          required: false
          schema:
            type: integer
          description: Amount of items per page
        - in: query
          name: sort
          required: false
          schema:
            type: string
          description: Sort direction asc, desc
          example: "asc"
        - in: query
          name: sort_by
          required: false
          schema:
            type: string
          description: Sorting per column name
          example: "title"
        
      responses:
        200:
          description: search results matching criteria
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    type: array
                    items: 
                      $ref: '#/components/schemas/EventDiscoveryModel'
          
  /events/{event_id}:
    get:
      tags:
        - events
      description: Shows event
      parameters:
        - in: path
          name: event_id
          required: true
          schema:
            type: integer
      responses:
        200:
          description: Ok
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    $ref: '#/components/schemas/EventDiscoveryModel'
        404:
          description: Not found
          
  /events/searchToken:
    get:
      tags:
        - events
        - search
      description: Search token for Algolia to search for events
      responses:
        200:
          description: OK
    
  /collections/{collection_id}:
    get:
      tags:
        - collections
      description: Shows collection
      parameters: 
        - in: path
          name: collection_id
          required: true
          schema:
            type: integer
          description: Gives only events back that are included in this collection.
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    $ref: '#/components/schemas/CollectionDiscoveryModel'
        404:
          description: Not found
          
  /agencies:
    get:
      tags:
        - agencies
      description: List all agencies
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    type: array
                    items: 
                      $ref: '#/components/schemas/AgencyDiscoveryModel'
  /agencies/{agency_id}:
    get:
      tags:
        - agencies
      description: List all agencies
      parameters: 
        - in: path
          name: agency_id
          required: true
          schema: 
            type: integer
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    $ref: '#/components/schemas/AgencyDiscoveryModel'
                      
  /genres:
    get:
      tags: 
        - genres
      description: List all genres
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  data: 
                    type: array
                    items:
                      $ref: '#/components/schemas/GenreDiscoveryModel'
  /genres/{genre_id}:
    get:
      tags: 
        - genres
      description: List all genres
      parameters: 
        - in: path
          name: genre_id
          schema:
            type: integer
          required: true
          example: 1
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  data: 
                    $ref: '#/components/schemas/GenreDiscoveryModel'
  /venues/{venue_id}:
    get:
      tags:
        - venues
      description: Shows venues
      parameters:
        - in: path
          name: venue_id
          required: true
          schema:
            type: integer
      responses:
        200:
          description: Ok
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    $ref: '#/components/schemas/VenueDiscoveryModel'
        404:
          description: Not found
components:
  schemas:
    EventDiscoveryModel:
      type: object
      required:
        - id
        - title
      properties:
        id:
          type: integer
          example: 1
        title:
          type: string
          example: Mustertitel
        subtitle:
          type: string
          example: Musteruntertitel
        start_date:
          type: string
          format: date-time
          example: "2018-12-13 23:54:51"
        end_date:
          type: string
          format: date-time
          example: "2018-12-13 23:54:51"
        description:
          type: string
          example: This is an example description. It can also be html.
        organizer:
          $ref: '#/components/schemas/OrganizerDiscoveryModel'
        venue:
          $ref: '#/components/schemas/VenueDiscoveryModel'
        genre:
          $ref: '#/components/schemas/GenreDiscoveryModel'
        media:
          type: object
          properties:
            header:
              type: object
              properties:
                id:
                  type: integer
                  example: 1
                url:
                  type: string
                  example: https://s3.aws.amazon.com/localTicketing/pictures/1.jpg
            logo:
              type: object
              properties:
                id:
                  type: integer
                  example: 1
                url:
                  type: string
                  example: https://s3.aws.amazon.com/localTicketing/pictures/1.jpg
    
    OrganizerDiscoveryModel:
      type: object
      required:
        - id
        - company_name
        - street
        - city
        - zipcode
        - country
      properties:
        id: 
          type: integer
          example: 1
        company_name:
          type: string
          example: Muster GmbH
        street:
          type: string
          example: Musterstr. 20
        city:
          type: string
          example: Musterstadt
        zipcode:
          type: string
          example: 48147
        country:
          type: string
          example: DE
          
    VenueDiscoveryModel:
      type: object
      required:
        - id
        - name
      properties:
        id:
          type: integer
          example: 1
        name:
          type: string
          example: Mustervenue
        street:
          type: string
          example: Musterstr. 20
        city:
          type: string
          example: Musterstadt
        zipcode:
          type: string
          example: 48147
        country:
          type: string
          example: DE
        latitude:
          type: string
          example: 50.71
        longitude:
          type: string
          example: 50.71
        media:
          type: object
          properties:
            header:
              type: object
              properties:
                id:
                  type: integer
                  example: 1
                url:
                  type: string
                  example: https://s3.aws.amazon.com/localTicketing/pictures/1.jpg
    
    CollectionDiscoveryModel:
      type: object
      required:
        - id
        - title
      properties:
        id:
          type: integer
          example: 1
        title:
          type: string
          example: Mustercollection
        subtitle:
          type: string
          example: Mustersubtitlecollection
        description:
          type: string
          example: This is a description. It can also be html.
          
    AgencyDiscoveryModel:
      type: object
      required:
        - id
        - company_name
      properties:
        id:
          type: integer
          example: 1
        company_name:
          type: string
          example: Wunderkasten GmbH
        street:
          type: string
          example: Musterstr. 20
        city:
          type: string
          example: Musterstadt
        zipcode:
          type: string
          example: 48147
        country:
          type: string
          example: DE
        lat_lng:
          type: string
          example: 50.71,74.21
    
    GenreDiscoveryModel:
      type: object
      required:
        - id
        - title
      properties:
        id:
          type: integer
          example: 1
        title:
          type: string
          example: Concerts
        description:
          type: string
          example: This is a description
        media:
          type: object
          properties:
            logo:
              type: object
              properties:
                id:
                  type: integer
                  example: 1
                url:
                  type: string
                  example: https://s3.aws.amazon.com/localTicketing/pictures/1.jpg